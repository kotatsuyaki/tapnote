import React from 'react';
import PropTypes from 'prop-types';
import {
  Container,
  Grow,
  Box,
  AppBar,
  Toolbar,
  Typography,
  Snackbar,
  IconButton,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles, createStyles, withTheme } from '@material-ui/core/styles';
import SaveSvgAsPng from 'save-svg-as-png';

import 'components/App.css';
import { StepIndicator } from 'components/StepIndicator';
import SheetDisplay from 'components/SheetDisplay';
import MeasureSetupDisplay from 'components/MeasureSetupDisplay';
import BeatInputDisplay from 'components/BeatInputDisplay';
import NextFab from 'components/NextFab';
import ExportDialog from 'components/ExportDialog';
import CopyDialog from 'components/CopyDialog';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeStep: 0,
      bpm: 0,
      timeSig: 4,
      startTime: 0,
      taps: [],
      exportDialogOpen: false,
      copySnackbarOpen: false,
      copySnackbarText: '',
      copyDialogOpen: false,
      abcString: '',
      svgParentId: '',
    };
  }

  render() {
    const { classes } = this.props;
    const {
      bpm,
      activeStep,
      timeSig,
      taps,
      startTime,
      exportDialogOpen,
      copySnackbarOpen,
      copySnackbarText,
      copyDialogOpen,
      abcString,
    } = this.state;

    return (
      <div className={classes.root}>
        <AppBar position="sticky" color="primary">
          <Toolbar>
            <Typography className={classes.title} variant="h6">
              Tapnote
            </Typography>
          </Toolbar>
        </AppBar>
        <Box className={classes.mainBox}>
          <StepIndicator activeStep={activeStep} />
          <NextFab
            disabled={!(bpm > 0)}
            ended={activeStep === 2}
            onNext={this.handleNextStep}
            onEnd={this.handleExport}
            onRestart={this.handleRestart}
          />
          <Container maxWidth="md">
            {/* The following elements are wrapped in three layers:
               1. The conditional rendering
               2. The transition effects
               3. <div> element
              Without the tags, the transition effects don't work and produce strange artifacts.
              Always keep the things inside <Grow> tags to be a single <div> element. */}
            {activeStep === 0 && (
              <Grow in={activeStep === 0}>
                <div>
                  <MeasureSetupDisplay
                    onBpmUpdate={this.handleBpmUpdate}
                    onTimeSignatureUpdate={this.handleTimeSignatureUpdate}
                    timeSig={timeSig}
                  />
                </div>
              </Grow>
            )}
            {activeStep === 1 && (
              <Grow in={activeStep === 1}>
                <div>
                  <BeatInputDisplay
                    bpm={bpm}
                    timeSig={timeSig}
                    onChange={this.handleBeatInputChange}
                  />
                </div>
              </Grow>
            )}
            {activeStep === 2 && (
              <Grow in={activeStep === 2}>
                <div>
                  <SheetDisplay
                    bpm={bpm}
                    timeSig={timeSig}
                    startTime={startTime}
                    taps={taps}
                    onAbcStringUpdate={this.handleAbcStringUpdate}
                    onSvgParentIdUpdate={this.handleSvgParentIdUpdate}
                  />
                </div>
              </Grow>
            )}
          </Container>
        </Box>
        <ExportDialog open={exportDialogOpen} onClose={this.handleExportClose} />
        <CopyDialog open={copyDialogOpen} onClose={this.handleCopyDialogClose} value={abcString} />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          open={copySnackbarOpen}
          autoHideDuration={5000}
          onClose={this.handleCopySnackbarClose}
          message={copySnackbarText}
          action={
            <React.Fragment>
              <IconButton size="small" color="inherit" onClick={this.handleCopySnackbarClose}>
                <CloseIcon fontSize="small" />
              </IconButton>
            </React.Fragment>
          }
        />{' '}
      </div>
    );
  }

  handleSvgParentIdUpdate = (id) => {
    this.setState({ svgParentId: id });
  };

  handleAbcStringUpdate = (abcString) => {
    this.setState({ abcString });
  };

  handleNextStep = () => {
    this.setState((state) => ({
      activeStep: state.activeStep <= 2 ? state.activeStep + 1 : state.activeStep,
    }));
  };

  handleBpmUpdate = (bpm) => {
    this.setState({
      bpm,
    });
  };

  handleTimeSignatureUpdate = (sig) => {
    this.setState({
      timeSig: sig,
    });
  };

  handleBeatInputChange = ({ startTime, taps }) => {
    this.setState({
      startTime,
      taps,
    });
  };

  handleCopySnackbarClose = () => {
    this.setState({ copySnackbarOpen: false });
  };

  handleExport = () => {
    this.setState({ exportDialogOpen: true });
  };

  handleExportClose = (value) => {
    this.setState({ exportDialogOpen: false });
    if (value === 'png') {
      this.exportAsPng();
    } else if (value === 'abc') {
      this.copyAbcString();
    }
  };

  exportAsPng = () => {
    const { svgParentId } = this.state;
    if (svgParentId !== 0) {
      SaveSvgAsPng.saveSvgAsPng(
        document.getElementById(svgParentId).children[0],
        'sheet-export.png',
        { backgroundColor: '#ffffff' }
      );
    }
  };

  copyAbcString = () => {
    this.setState({ copyDialogOpen: true });
  };

  handleCopyDialogClose = () => {
    this.setState({ copyDialogOpen: false });
  };

  handleRestart = () => {
    this.setState({
      activeStep: 0,
      bpm: 0,
      timeSig: 4,
      startTime: 0,
      taps: [],
      exportDialogOpen: false,
      copySnackbarOpen: false,
      abcString: '',
      svgParentId: '',
    });
  };
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

const styles = (_theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      display: 'flex',
      flexFlow: 'column',
      minHeight: '100vh',
      backgroundColor: '#eeeeee',
    },
    title: {
      flexGrow: 1,
    },
    mainBox: {
      height: '100%',
    },
  });

export default withTheme(withStyles(styles)(App));
