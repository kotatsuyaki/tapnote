import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import {
  Tooltip,
  Slider,
  Grid,
  Card,
  CardContent,
  Typography,
  TextField,
  MenuItem,
} from '@material-ui/core';

// For sheet music rendering
import { nanoid } from 'nanoid';
import abcjs from 'abcjs';

import { convert } from 'convertUtils';

class SheetDisplay extends React.Component {
  constructor(props) {
    super(props);

    this.id = nanoid();
    this.prevSizeClass = this.currentSizeClass();
    this.state = {
      precision: 2,
      unit: 4,
      empty: false,
    };

    if (this.props.onSvgParentIdUpdate) {
      this.props.onSvgParentIdUpdate(this.id);
    }
  }

  render() {
    const id = this.id;
    const { classes } = this.props;
    const { unit, empty } = this.state;

    const precisionMarks = [
      { value: 1, label: '1' },
      { value: 2, label: '2' },
      { value: 4, label: '4' },
    ];
    const unitMarks = [
      { value: 2, label: 'Half note' },
      { value: 4, label: 'Quarter note' },
      { value: 8, label: 'Eighth note' },
      { value: 16, label: 'Sixteenth note' },
    ];

    return (
      <div>
        <Grid container spacing={3} className={classes.mainGrid}>
          <Grid item xs={12} md={8}>
            <Card className={classes.root}>
              <CardContent>
                {empty && (
                  <Typography variant="h6" color="gray" align="center">
                    No notes to show
                  </Typography>
                )}
                <div id={id} className={classes.score}></div>
              </CardContent>
            </Card>
          </Grid>
          <Grid item xs={12} md={4}>
            <Card>
              <CardContent>
                <Typography variant="h6" gutterBottom>
                  Options
                </Typography>
                <Typography gutterBottom>
                  Precision
                  <Tooltip title="Number of snapping points per beat">
                    <HelpOutlineIcon fontSize="small" color="primary" />
                  </Tooltip>
                </Typography>
                <Slider
                  defaultValue={2}
                  valueLabelDisplay="auto"
                  onChangeCommitted={this.handlePrecisionChange}
                  min={1}
                  max={4}
                  step={null}
                  marks={precisionMarks}
                />
                <Typography gutterBottom>
                  Beat unit
                  <Tooltip title="Unit ">
                    <HelpOutlineIcon fontSize="small" color="primary" />
                  </Tooltip>
                </Typography>
                <TextField
                  id="standard-select-currency"
                  select
                  value={unit}
                  onChange={this.handleUnitChange}
                  fullWidth={true}
                >
                  {unitMarks.map(({ value, label }) => (
                    <MenuItem key={value} value={value}>
                      {label}
                    </MenuItem>
                  ))}
                </TextField>{' '}
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </div>
    );
  }

  componentDidMount() {
    this.renderSheet();
    window.addEventListener('resize', this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  abcstring = '';
  renderSheet = () => {
    if (this.abcstring === '') {
      this.updateAbcstring();
    }
    console.log(this.abcstring);
    const id = this.id;
    const options = {
      wrap: {
        minSpacing: 1.8,
        maxSpacing: 2.8,
        preferredMeasuresPerLine: this.currentSizeClass() === 'small' ? 3 : 4,
      },
      dragging: true,
      selectTypes: 'note',
      clickListener: () => {},
      responsive: 'resize',
      staffwidth: this.currentSizeClass() === 'small' ? 300 : 500,
    };
    abcjs.renderAbc(id, this.abcstring, options);
  };

  updateAbcstring = () => {
    const { taps, bpm, timeSig, startTime, onAbcStringUpdate } = this.props;
    const { precision, unit } = this.state;
    /** @type {string} */
    this.abcstring = convert(taps, startTime, bpm, timeSig, precision, unit);
    if (onAbcStringUpdate) {
      onAbcStringUpdate(this.abcstring);
    }
    if (!this.abcstring.includes('|')) {
      this.setState({ empty: true });
    } else {
      this.setState({ empty: false });
    }
  };

  handlePrecisionChange = (_e, value) => {
    this.setState({ precision: value }, () => {
      this.updateAbcstring();
      this.renderSheet();
    });
  };

  handleUnitChange = (e) => {
    this.setState(
      {
        unit: e.target.value,
      },
      () => {
        this.updateAbcstring();
        this.renderSheet();
      }
    );
  };

  currentSizeClass = () => {
    if (window.innerWidth < 960) {
      return 'small';
    } else {
      return 'large';
    }
  };

  handleWindowSizeChange = (_e) => {
    if (this.currentSizeClass() !== this.prevSizeClass) {
      this.renderSheet();
      this.prevSizeClass = this.currentSizeClass();
    }
  };
}

SheetDisplay.propTypes = {
  bpm: PropTypes.number.isRequired,
  timeSig: PropTypes.number.isRequired,
  startTime: PropTypes.number.isRequired,
  taps: PropTypes.arrayOf(PropTypes.number),
  onAbcStringUpdate: PropTypes.func,
  onSvgParentIdUpdate: PropTypes.func,
};

const styles = (theme) => ({
  root: {},
  score: {
    minWidth: '100%',
  },
  mainGrid: {
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column-reverse',
    },
  },
});

export default withStyles(styles)(SheetDisplay);
