import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, createStyles } from '@material-ui/core/styles';
import { Fab, Zoom } from '@material-ui/core';
import DoneIcon from '@material-ui/icons/Done';

class NextFab extends React.Component {
  render() {
    const { classes, ended, theme, disabled } = this.props;
    const fabs = [
      {
        color: 'primary',
        icon: null,
        text: 'Next',
        showWhenEnded: false,
      },
      {
        color: 'primary',
        icon: <DoneIcon />,
        text: 'Export',
        showWhenEnded: true,
      },
    ];
    const transitionDuration = {
      enter: theme.transitions.duration.shortest,
      exit: theme.transitions.duration.shortest,
    };

    return fabs
      .map((fab) => {
        return (
          <Zoom
            in={ended === fab.showWhenEnded}
            timeout={transitionDuration}
            style={{
              transitionDelay: `${ended === fab.showWhenEnded ? transitionDuration.exit : 0}ms`,
            }}
            unmountOnExit
          >
            <Fab
              variant="extended"
              color={fab.color}
              className={classes.fab}
              onClick={this.handleClicked}
              disabled={disabled || false}
            >
              {fab.text}
            </Fab>
          </Zoom>
        );
      })
      .concat([
        <Zoom
          in={ended}
          timeout={transitionDuration}
          style={{
            transitionDelay: `${ended ? transitionDuration.exit : 0}ms`,
          }}
          unmountOnExit
        >
          <Fab variant="extended" className={classes.secondFab} onClick={this.handleRestart}>
            Restart
          </Fab>
        </Zoom>,
      ]);
  }

  handleClicked = () => {
    const { ended, onNext, onEnd } = this.props;
    if (!ended && onNext) {
      onNext();
    }
    if (ended && onEnd) {
      onEnd();
    }
  };

  handleRestart = () => {
    const { onRestart } = this.props;
    if (onRestart) {
      onRestart();
    }
  };
}

NextFab.propTypes = {
  disabled: PropTypes.bool,
  ended: PropTypes.bool.isRequired,
  onNext: PropTypes.func,
  onEnd: PropTypes.func,
  onRestart: PropTypes.func,
};

const styles = createStyles({
  secondFab: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 80,
    position: 'fixed',
    zIndex: 1,
  },
  fab: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    position: 'fixed',
    zIndex: 1,
  },
});
export default withStyles(styles, { withTheme: true })(NextFab);
