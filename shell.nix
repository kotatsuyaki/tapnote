{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    zsh nodejs
    nodePackages.npm nodePackages.serve
  ];
}
